// 选项卡
$('#list li').click(function () {
  $(this).addClass("active").siblings().removeClass("active");
  $('#content ul').eq($(this).index()).addClass("show").siblings().removeClass("show");
});



// 手风琴
$(document).ready(function () {
  $("dd:not(:first)").hide(); //这句话的意思就是除了第一个以外全部都隐藏
  $("dt .icontxt").click(function () {
    $("dd:visible").slideUp("slow");
    $(this).parent().next().slideDown("slow");
    return false;
  });
});

// 返回上一级
$('.goback').click(function(){
  return window.history.go(-1);//页面后退一步
});