//轮播
//把轮播效果封装成函数
function run(i) { //i是显示第几张图片的索引从0开始数
  //图片轮播效果,每一次向左移动..px
  $('.banner .ul').animate({
    left: '-' + i * 6.9 + 'rem',
  }, 500);
  //小圆点效果
  $('.banner .bar li').eq(i).addClass('cur').siblings().removeClass('cur');
}
var i = 0; //显示图片的索引初始值
//获取图片的总数量
var len = $('.banner .ul li').length;
//做自动轮播功能
function autoplay() {
  if (i == len) {
    i = 0; //如果轮播到最后一张就返回到第一张
  }
  run(i);
  i++;
}
var lunbo = setInterval(autoplay, 4000);
//点击小圆点跳转到指定图片
$('.banner .bar li').click(function () {
  i = $(this).index();
  run(i);
})




// 倒计时
var hours = document.querySelector(".hours");
var minutes = document.querySelector(".minutes");
var seconds = document.querySelector(".seconds");
function bu(z){ //补零函数
  if(z<10){ //小于10就补零
    z='0'+z;
  }
  return z;
}
function formDate(time1,time2){
  var t1 = time1.getTime()
  var t2 = time2.getTime()
  var t = Math.floor((t1-t2)/1000)//现在距离2022还有多少秒
  var date = Math.floor(t/(60*60*24))//日
  var modul = t%(60*60*24)//求余(用距离2022剩下的秒来求余天，剩下的就是时)
  var h = Math.floor(modul/(60*60))//时
  modul = t%(60*60)
  var m = Math.floor(modul/(60))//分
  modul = t%60
  var s = Math.floor(modul)//秒
  // el.innerText = "距离2022还剩:"+date+"日"+h+"时"+m+"分"+s+"秒";
  hours.innerText = bu(h);
  minutes.innerText = bu(m);
  seconds.innerText = bu(s);
}
setInterval(function(){
  var time1 = new Date(2022,00,01)//2022
  var time2 = new Date()//现在时间
  formDate(time1,time2)
},1000)